import {form, user, psw, endpoint} from './elements.js'
console.log(form); 

async function postPlayerCheck(name,psw){
    return fetch(endpoint, {
        method : 'post',
        headers : {
            'Content-Type' : 'application/json'
        },
        body : JSON.stringify({username: name, password: psw})
    });
}

function handleInput(e) {
    e.preventDefault();
    let tres;
    const uname = user.value;
    const upsw = psw.value;
    postPlayerCheck(uname,upsw)
    .then(res => {
        tres = res;
        return res.json()})
    .then(data => {
        console.log(data);
        localStorage.setItem('key',data.id);
        if(tres.status === 200){
            window.location = 'game.html';
        }
    });
}

form.addEventListener('submit', handleInput);


