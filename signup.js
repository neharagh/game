import {form, user, psw, email} from './elements.js'
const endpoint = 'http://10.154.4.174/players';
console.log(form); 

async function postPlayer(name,mail,psw){
    return fetch(endpoint, {
        method : 'post',
        headers : {
            'Content-Type' : 'application/json'
        },
        body : JSON.stringify({username: name, email: mail, password: psw})
    });
}

function handleInput(e) {
    e.preventDefault();
    const uname = user.value;
    const uemail = email.value;
    const upsw = psw.value;
    postPlayer(uname,uemail,upsw).then(res => {
        console.log(res.json());
        if(res.status === 200){
            window.location = 'indexlogin.html';
        }
    });
}

form.addEventListener('submit', handleInput);
